from django.urls import path

from . import views

urlpatterns = [
    path(
        "mdat-tld-autocomplete",
        views.MdatTopLevelDomainsAutocomplete.as_view(),
        name="mdat-tld-autocomplete",
    ),
]
