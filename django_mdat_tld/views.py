from dal import autocomplete
from django.db.models import Q

from django_mdat_tld.django_mdat_tld.models import *


class MdatTopLevelDomainsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = MdatTopLevelDomains.objects.all()

        if self.q:
            qs = qs.filter(
                Q(name=self.q) | Q(punycode=self.q),
            )

        return qs
