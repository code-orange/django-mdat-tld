from django.db import models


class MdatTopLevelDomainTypes(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "mdat_tld_types"


class MdatTopLevelDomains(models.Model):
    name = models.CharField(max_length=200, unique=True)
    type = models.ForeignKey(MdatTopLevelDomainTypes, models.DO_NOTHING)
    punycode = models.CharField(max_length=200, unique=True, null=True)
    right_to_left = models.BooleanField(default=False)
    sponsor = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "mdat_tld"
